﻿namespace BlazorWebApp.Extensions
{
    public class AddHeadersDelegatingHandler : DelegatingHandler
    {
        public AddHeadersDelegatingHandler() : base(new HttpClientHandler())
        {
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            request.Headers.Add("User-Agent", "(WebClientDevTest, josemanuel.0926@gmail.com)");

            return base.SendAsync(request, cancellationToken);
        }
    }
}
