﻿using BlazorWebApp.Shared.Interfaces;
using BlazorWebApp.Shared.Models;

namespace BlazorWebApp.Shared.Services
{
    public class WeathersService : IWeathersService
    {
        private readonly HttpClient _httpClient;

        public WeathersService(HttpClient httpClient)
        {
            this._httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }
        public async Task<WeatherApiResponse?> GetWeatherForecast(string zone)
        {
            try
            {
                return await _httpClient.GetFromJsonAsync<WeatherApiResponse>($"{zone}/forecast")!;
            }
            catch (HttpRequestException exception)
            {
                Console.WriteLine(exception.Message);
                return null;
            }
        }
    }
}
