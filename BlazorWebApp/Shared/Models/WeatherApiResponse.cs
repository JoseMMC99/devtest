﻿namespace BlazorWebApp.Shared.Models
{
    public class WeatherApiResponse
    {
        public string Type { get; set; } = string.Empty;
        public Object Geometry { get; set; } = default!;
        public WeatherForecast Properties { get; set; } = default!;

    }
}
