﻿namespace BlazorWebApp.Shared.Models
{
    public class WeatherPeriod
    {
        public int Number { get; set; } = 0;
        public string Name { get; set; } = string.Empty;
        public string DetailedForecast { get; set; } = string.Empty;
    }
}
