﻿namespace BlazorWebApp.Shared.Models
{
    public class WeatherForecast
    {
        public string Zone { get; set; } = string.Empty;
        public string Updated { get; set; } = string.Empty;
        public WeatherPeriod[] Periods { get; set; } = default!;
    }
}
