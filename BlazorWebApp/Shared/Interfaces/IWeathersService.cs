﻿using BlazorWebApp.Shared.Models;

namespace BlazorWebApp.Shared.Interfaces
{
    public interface IWeathersService
    {
        public Task<WeatherApiResponse?> GetWeatherForecast(string zone);
    }
}
