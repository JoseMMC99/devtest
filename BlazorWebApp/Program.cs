using BlazorWebApp.Extensions;
using BlazorWebApp.Shared.Interfaces;
using BlazorWebApp.Shared.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
builder.Services.AddSingleton<IWeathersService, WeathersService>();
//builder.Services.AddHttpClient<IWeathersService, WeathersService>(sp => new HttpClient(new AddHeadersDelegatingHandler())
//.BaseAddress = new Uri(builder.Configuration.GetConnectionString("WeatherApi")));
builder.Services.AddTransient(sp => new HttpClient(new AddHeadersDelegatingHandler())
{
    BaseAddress = new Uri(builder.Configuration.GetConnectionString("WeatherApi"))
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseRouting();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

app.Run();
